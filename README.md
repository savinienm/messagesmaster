# Documentation de l'application Messages Master

## Installation du projet

Messages Master a été réalisé avec le framework Symfony dans sa version 5.4 et avec php 8.1.13. Pour la mise en forme, j'ai utilisé le framework CSS Bootstrap 5.
Le but de l'application consiste à envoyer des messages sur les plateformes de Discord et Slack grâce à l'usage de webhook.
Afin de mettre en place le projet sur votre machine il vous faudra :

1. Cloner le projet sur le dépôt git
2. Récupérer le fichier dump de la base de donnée et importer la base de données (le fichier dump_messages_master est à la racine du projet)
3. Lancer la commande ```composer install``` et ```npm install``` puis ```npm run build```
4. Créer un fichier .env.local ou enrichir le fichier .env

Certaines variables du fichier .env doivent être modifiées :
- DATABASE_URL doit comprendre le bon utilisateur, le bon mot de passe et le bon nom de base de données
- DISCORD_DSN doit être modifié avec les informations que vous obtiendrez en créant un webhook pour votre serveur discord
- SLACK_DSN doit être modifié également à l'aide des informations de configuration de votre serveur Slack.

Enfin, en supposant que les étapes précédentes sont validées, le projet est accessible en localhost et sur le port 8000 en réalisant la commande ```symfony server:start -d``` puis ```symfony console messenger:consume async -vv``` (si vous n'avez pas Symfony CLI la commande ```symfony console``` peut être remplacée par ```php bin/console```)

## Configuration

Pour que le projet fonctionne, il est nécessaire de créer au préalable un serveur Discord et Slack afin de configurer un Webhook.

### Webhook Discord

Pour obtenir un Webhook pour votre serveur Discord il vous faut accéder aux paramètres du serveur que vous avez préalablement créé, puis accéder à la partie Intégrations dans l'onglet de gauche, cliquer sur "Webhooks" et "Nouveau Webhook". Cela va créer un webhook que vous pourrez paramétrer (nom, salon où il interviendra) et il sera possible de copier l'URL du Webhook qui contient toutes les informations nécessaires (token, id).

### Webhook Slack

Pour Slack, il vous faut créer un espace de travail, puis créer une application. Lorsque vous êtes sur la page des paramètres de votre application (bot), cliquez sur OAuth & Permissions dans l'onglet de gauche. Vous aurez un Bot User OAuth Token qui est le token à renseigner dans le fichier .env (il doit commencer par xoxb-). Plus bas, dans la partie Scopes, ajoutez les scopes (droits) qui vous intéresse (notamment lecture/écriture des channels et des chats). Il reste à préciser dans le .env au niveau de la variable SLACK_DSN le channel où vous souhaitez envoyer des messages.

## Choix techniques

Pour ce projet j'ai souhaité mettre en place une architecture simple et de manière général faire au plus simple, étant donné que cela fait plus d'un an et demi que je n'ai pas fait de php et que je n'ai jamais vu auparavant les notions de webhook, messages, programmation d'une date d'envoi, etc. C'est pour cette raison que je n'ai pas construit l'application sous forme d'API avec un framework frontend particulier.
J'ai souhaité dans un premier temps mettre en place une validation de compte par email, mais Symfony (à ma connaissance) ne permet pas l'envoie d'email sans la mise en place d'un serveur SMTP. J'ai donc choisi de mettre en place en base de données un trigger permettant que le premier compte créé dans l'application soit administrateur et vérifié. J'ai mis par la suite en place une gestion des utilisateurs permettant à l'administrateur de cocher manuellement la case de vérification d'un utilisateur.
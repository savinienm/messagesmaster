<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Form\AccountType;
use Symfony\Component\HttpFoundation\RequestStack;

#[Route('/account')]
class AccountController extends AbstractController
{
    private $requestStack;

    public function __construct(RequestStack $requestStack){
        $this->requestStack = $requestStack;
    }

    #[Route('/', name: 'app_account', methods: ['GET'])]
    public function show(UserRepository $userRepository): Response
    {
        $userEmail = $this->getUser()->getUserIdentifier();
        $user = $userRepository->findOneByEmail($userEmail);
        return $this->render('account/account.html.twig', [
            'user' => $user,
        ]);
    }

    #[Route('/edit', name: 'app_account_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, UserRepository $userRepository, UserPasswordHasherInterface $userPasswordHasher): Response
    {
        $userEmail = $this->getUser()->getUserIdentifier();
        $user = $userRepository->findOneByEmail($userEmail);
        $form = $this->createForm(AccountType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $form->get('plainPassword')->getData();
            if($password === null) {
                $user->setPassword($user->getPassword());
            } else {
                $user->setPassword(
                    $userPasswordHasher->hashPassword(
                        $user,
                        $password
                    )
                );
            }
            $userRepository->save($user, true);

            return $this->redirectToRoute('app_account', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('account/edit.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }

    #[Route('/', name: 'app_account_delete', methods: ['POST'])]
    public function delete(Request $request, UserRepository $userRepository): Response
    {
        $userEmail = $this->getUser()->getUserIdentifier();
        $user = $userRepository->findOneByEmail($userEmail);
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $userRepository->remove($user, true);
        }

        $this->requestStack->getSession()->invalidate();
        $this->container->get('security.token_storage')->setToken(null);

        return $this->redirectToRoute('app_login');
    }

}

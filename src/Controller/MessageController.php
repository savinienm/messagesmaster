<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Message\SendNotification;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DelayStamp;
use Symfony\Component\Messenger\Envelope;
use App\Form\MessageType;
use App\Repository\UserRepository;
use App\Form\MessengerMessageType;
use App\Repository\MessageSentRepository;
use App\Repository\MessengerMessageRepository;
use App\Entity\MessengerMessage;

#[Route('/message')]
class MessageController extends AbstractController
{
    #[Route('/', name: 'app_message', methods: ['GET'])]
    public function index(MessageSentRepository $messageSentRepository): Response
    {
        return $this->render('message/index.html.twig', [
            'messages' => $messageSentRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_message_new', methods: ['GET', 'POST'])]
    public function new(Request $request, MessageBusInterface $bus, UserRepository $userRepository): Response
    {
        $userEmail = $this->getUser()->getUserIdentifier();
        $user = $userRepository->findOneByEmail($userEmail);

        $form = $this->createForm(MessageType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $user->isVerified()) {

            $isInstant = $form->get('is_instant')->getData();

            $deliveryTimestamp = $form->get('delivery_date')->getData()->getTimeStamp();

            // Calcule l'écart en ms entre le moment où le formulaire est soumis et la date choisi par l'utilisateur -1h (3600 secondes) pour l'heure d'hiver
            $delay = (($deliveryTimestamp - time()) - 3600) * 1000;

            $channels = $form->get('channels')->getData();
            $isOnDiscord = in_array('discord', $channels);
            $isOnSlack = in_array('slack', $channels);

            if($isInstant || $delay <= 0) {
                $bus->dispatch(new SendNotification($form->get('message_content')->getData(), $isOnDiscord, $isOnSlack));
            } else {
                $bus->dispatch(new Envelope(new SendNotification($form->get('message_content')->getData(), $isOnDiscord, $isOnSlack), [
                    new DelayStamp($delay),
                ]));
            }

            return $this->redirectToRoute('app_message_queue', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('message/new.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/queue', name: 'app_message_queue', methods: ['GET'])]
    public function queue(MessengerMessageRepository $messengerMessageRepository): Response
    {
        return $this->render('message/queue.html.twig', [
            'messages' => $messengerMessageRepository->findAll(),
        ]);
    }

    #[Route('/queue/{id}/edit', name: 'app_message_queue_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, MessengerMessage $messengerMessage, MessengerMessageRepository $messengerMessageRepository): Response
    {

        $form = $this->createForm(MessengerMessageType::class, $messengerMessage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $isInstant = $form->get('is_instant')->getData();
            $availableDate = ($form->get('availableAt')->getData()->getTimeStamp());

            if($isInstant || $availableDate <= time()) {
                $date = new \DateTimeImmutable();
                $timestamp = time() + 120;
                $newDate = $date->setTimestamp($timestamp);
                $messengerMessage->setAvailableAt($newDate);
            } else {
                $messengerMessage->setAvailableAt(
                    (new \DateTimeImmutable())->setTimestamp($availableDate - 3600)
                );
            }

            $messengerMessageRepository->save($messengerMessage, true);

            return $this->redirectToRoute('app_message_queue', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('message/edit.html.twig', [
            'message' => $messengerMessage,
            'form' => $form,
        ]);
    }

    #[Route('/queue/{id}', name: 'app_message_queue_delete', methods: ['POST'])]
    public function delete(Request $request, MessengerMessage $messengerMessage, MessengerMessageRepository $messengerMessageRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$messengerMessage->getId(), $request->request->get('_token'))) {
            $messengerMessageRepository->remove($messengerMessage, true);
        }

        return $this->redirectToRoute('app_message_queue', [], Response::HTTP_SEE_OTHER);
    }

}

<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

class MessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('message_content', TextareaType::class, [
                'required' => true
            ])
            ->add('delivery_date', DateTimeType::class, [
                'years' => range(date('Y'), date('Y')+5)
            ])
            ->add('is_instant', CheckboxType::class, [
                'required' => false
            ])
            ->add('channels', ChoiceType::class, array(
                'choices' => array(
                    'Discord channel' => 'discord',
                    'Slack channel' => 'slack'
                ),
                'required' => true,
                'expanded' => true,
                'multiple' => true
            ))
            ->addEventListener(
                FormEvents::PRE_SUBMIT,
                function (FormEvent $event) {
                    if (!isset($event->getData()['channels'])) {
                        $event->getForm()->addError(new FormError('At least one option must be chosen on channels field'));
                    }
                })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}

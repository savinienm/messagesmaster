<?php

namespace App\Form;

use App\Entity\MessengerMessage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class MessengerMessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('availableAt', DateTimeType::class, [
                'input' => 'datetime_immutable',
                'years' => range(date('Y'), date('Y') + 5),
                'label' => 'Choose a sending date and time',
                'mapped' => false
            ])
            ->add('is_instant', CheckboxType::class, [
                'required' => false,
                'mapped' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MessengerMessage::class,
        ]);
    }
}

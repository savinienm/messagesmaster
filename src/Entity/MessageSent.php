<?php

namespace App\Entity;

use App\Repository\MessageSentRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MessageSentRepository::class)]
class MessageSent
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $message = null;

    #[ORM\Column]
    private ?bool $isOnDiscord = null;

    #[ORM\Column]
    private ?bool $isOnSlack = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $deliveredAt = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function isIsOnDiscord(): ?bool
    {
        return $this->isOnDiscord;
    }

    public function setIsOnDiscord(bool $isOnDiscord): self
    {
        $this->isOnDiscord = $isOnDiscord;

        return $this;
    }

    public function isIsOnSlack(): ?bool
    {
        return $this->isOnSlack;
    }

    public function setIsOnSlack(bool $isOnSlack): self
    {
        $this->isOnSlack = $isOnSlack;

        return $this;
    }

    public function getDeliveredAt(): ?\DateTimeImmutable
    {
        return $this->deliveredAt;
    }

    public function setDeliveredAt(\DateTimeImmutable $deliveredAt): self
    {
        $this->deliveredAt = $deliveredAt;

        return $this;
    }
}

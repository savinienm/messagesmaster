<?php

namespace App\MessageHandler;

use App\Message\SendNotification;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\NotifierInterface;
use App\Repository\MessageSentRepository;
use App\Entity\MessageSent;

final class SendNotificationHandler implements MessageHandlerInterface
{

    private $notifier;
    private $messageSentRepository;

    public function __construct(NotifierInterface $notifier, MessageSentRepository $messageSentRepository)
    {
        $this->notifier = $notifier;
        $this->messageSentRepository = $messageSentRepository;
    }

    public function __invoke(SendNotification $message)
    {
        $channels = [];

        if($message->getIsOnDiscord()) {
            array_push($channels, 'chat/discord');
        }
        if($message->getIsOnSlack()) {
            array_push($channels, 'chat/slack');
        }

        $notification = new Notification(
            $message->getMessage(),
            $channels
        );

        try {
            $this->notifier->send($notification);

            $messageSent = new MessageSent();
            $messageSent->setMessage($message->getMessage());
            $messageSent->setIsOnSlack($message->getIsOnSlack());
            $messageSent->setIsOnDiscord($message->getIsOnDiscord());

            $date = new \DateTimeImmutable();
            // 3600 s (1h) pour le décalage à l'heure d'hiver
            $timestamp = time() + 3600;
            $newDate = $date->setTimestamp($timestamp);
            $messageSent->setDeliveredAt($newDate);
            $this->messageSentRepository->save($messageSent, true);

        } catch (\Exception $e) {
            var_dump($e);
        }
    }
}

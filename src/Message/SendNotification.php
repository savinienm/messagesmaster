<?php

namespace App\Message;

final class SendNotification
{
    /*
     * Add whatever properties and methods you need
     * to hold the data for this message class.
     */

    private string $message;

    private bool $isOnDiscord;

    private bool $isOnSlack;


    public function __construct(string $message, bool $isOnDiscord, bool $isOnSlack)
    {
        $this->message = $message;
        $this->isOnDiscord = $isOnDiscord;
        $this->isOnSlack = $isOnSlack;
    }

   public function getMessage(): string
   {
       return $this->message;
   }
   
   public function getIsOnDiscord(): bool
   {
       return $this->isOnDiscord;
   }

   public function getIsOnSlack(): bool
   {
       return $this->isOnSlack;
   }
}